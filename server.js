var cluster = require('cluster');

var express = require("express");
var app = express(),
fs = require("fs");


global.passport = require('passport');
global.async = require('async');
global.crypto = require('crypto');
global.request = require('request');
global.jwt = require('jsonwebtoken');

var path = require("path");
global.appRoot = path.resolve(__dirname);

var cors = require('cors');
var request = require('request');

global.config = require('./config/config');
global.messages = require('./config/message');


var yamlconfig = require('yaml-config');
var settings = yamlconfig.readConfig(path.join(__dirname, 'config/config.yaml'));

var logging = require('./utils/LogWrapper');
global.logger = logging.createLogger(settings.logs);

global.mongoose = require(appRoot + '/utils/MongoWrapper').mongoose;
mongoose.Promise = require('bluebird');
global.uniqueValidator = require('mongoose-unique-validator');


global.CommonWrapper = require(appRoot + '/utils/CommonWrapper');
global.ErrorWrapper = require(appRoot + '/utils/ErrorWrapper');
global.SuccessWrapper = require(appRoot + '/utils/SuccessWrapper');


http = require("http").createServer(app);


var serverDown = '{"error":"503"}';
var internalError = '{"error":"500"}';


app.set('views', __dirname + '/public');

// used below code to render html files
app.engine('html', require('ejs').renderFile);

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, '/public')));

//Mention the path name which should be redered in the browser
var routePath = ['/day/*'];

app.get(routePath, function(req, res) {
  res.sendFile(__dirname + '/public/index.html')
});

/**
 * Node server is listening on a port defined here.
 **/
http.listen(config.port);


//app.use(require('connect').bodyParser());
bodyParser = require("body-parser");
app.use(bodyParser());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));


/**
 * Node application configuration for all REST urls. This code will set the REST urls to node global variable.
 **/
app.post('/config', function(req, res){
    res.send(req.body.urls);
});

var resources = require('node-resources');
resources.registerRoutes(app, {path: __dirname + "/resources", pattern: "[folder].routes.js"});



    
