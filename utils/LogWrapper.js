/*global async: true, require: true, console: true, process: true, logger: true, messages: true, SuccessWrapper: true, jwt: true, secret: true, config: true, ErrorWrapper: true, module: true, params: true  */

/**
 * Created by Dhiman on 2/1/2017.
*/

'use strict';

var fs = require('fs')
  , path = require('path')
  , yamlconfig = require('yaml-config')
  , bunyan = require('bunyan');


exports.createLogger = createLogger;


/*
 * configure and start logging
 * @param {Object} yamlconfig The configuration object for defining dir: log directory, level: loglevel
 * @return the created logger instance
 */
function createLogger (yamlconfig) {

  var pkg = require(path.join(appRoot, 'package'))
    , appName = pkg.name
    , appVersion = pkg.version
    , logDir = yamlconfig.dir || path.join(appRoot, 'logs')
    , logFile = path.join(logDir, appName + '-log.log')
    , logErrorFile = path.join(logDir, appName + '-errors.log')
    , logLevel = yamlconfig.level || 'debug';

  // Create log directory if it doesnt exist
  if (! fs.existsSync(logDir)) fs.mkdirSync(logDir);

  // Log to console and log file
  var log = bunyan.createLogger({
      name: appName, 
        streams: [ 
            {
                stream: process.stdout,
                level: 'warn'
            },
            { 
                path: logFile, 
                level: logLevel,  
                period: '1d'
            }, 
            { 
                path: logErrorFile, 
                level: 'error'
            }
        ], 
        serializers: bunyan.stdSerializers
  });

  log.info('Starting ' + appName + ', version ' + appVersion);
  log.info('Environment set to ' + process.env.NODE_ENV);
  log.debug('Logging setup completed.');
  
  return log;
}