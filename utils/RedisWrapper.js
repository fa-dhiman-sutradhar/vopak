/*global async: true, require: true, console: true, process: true, logger: true, messages: true, SuccessWrapper: true, jwt: true, secret: true, config: true, ErrorWrapper: true, module: true, params: true  */

/**
 * Created by Dhiman on 2/1/2017.
*/


var redisClient = require(appRoot + '/config/redis_database').redisClient;
var TOKEN_EXPIRATION = 60;
var TOKEN_EXPIRATION_SEC = TOKEN_EXPIRATION * 60;
 
exports.expireToken = function (headers) {
    var token = getToken(headers);
 
    if (token != null) {
        redisClient.set(token, { is_expired: true });
        redisClient.expire(token, TOKEN_EXPIRATION_SEC);
    }
};
 
var getToken = function (headers) {
    
    if (headers && headers.authorization) {
        var authorization = headers.authorization;
        /*var part = authorization.split(' ');
 
        if (part.length == 2) {
            var token = part[1];
 
            return part[1];
        }
        else {
            return null;
        }*/
        return authorization;
    } else {
        return null;
    }
};
exports.dataExist = function (key, callback) {
    redisClient.get(key, function (err, reply) {
        
        if (reply == null) {
            callback(false);
        } else {
            callback(true);
        }
    });
};

exports.setData = function (key, value) {
    
    redisClient.set(key, JSON.stringify(value), function (err, reply) {
        if (err)
            console.log(err);
        if (reply) {
            
        }
    });
};

exports.getData = function (key, callback) {
    
    redisClient.get(key, function (err, reply) {
        
        callback(reply);
        
    });
};

exports.usernameExist = function (userId) {
    
    redisClient.get(userId, function (err, reply) {
        
        if (reply == null) {
            return false;
        } else {
            return true;
        }
    });
};

exports.User = function(userId, socketId) {
    this.userId = userId;
    this.socketId = socketId;
};

exports.setUser = function (userId, socketId) {
    
    redisClient.set(userId, socketId, function(err, reply) {
        if(err) console.log(err);
        if (reply) {
            
        }
    });
};

exports.removeUser = function (userId) {
    console.log("userId : " + userId);
    redisClient.del(userId);
    //redisClient.srem(userId);
};

exports.setTokenWithData = function (token, data, ttl, callback) {
    if (token == null) throw new Error('Token is null');
    if (data != null && typeof data !== 'object') throw new Error('data is not an Object');
 
    var userData = data || {};
    userData._ts = new Date();
 
    var timeToLive = ttl || auth.TIME_TO_LIVE;
    if (timeToLive != null && typeof timeToLive !== 'number') throw new Error('TimeToLive is not a Number');
 
    redisClient.set(token, JSON.stringify(userData), function(err, reply) {
        if (err) callback(err);
 
        if (reply) {
            redisClient.expire(token, timeToLive, function(err, reply) {
                if (err) callback(err);
 
                if (reply) {
                    callback(null, true);
                }
                else {
                    callback(new Error('Expiration not set on redis'));
                }
            });
        }
        else {
            callback(new Error('Token not set in redis'));
        }
    });
 
};

// Middleware for token verification
exports.verifyToken = function (req, res, next) {
    var token = getToken(req.headers);
    redisClient.get(token, function (err, reply) {
        
        //next();
        
        if (err) {
            console.log(err);
            return res.send(500);
        }
 
        if (reply) {
            
            var user = JSON.parse(reply);
            
            if(user.userid == req.headers.userid){
                next();
            }else{
                res.send(new restify.errors.UnauthorizedError({
                    message: "Un-authorise access"
                }));
            }
            
        }else {
            res.send(new restify.errors.UnauthorizedError({
                message: "Un-authorise access"
            }));
        }
 
    });
};

exports.RemoveToken = function(key) {    
    redisClient.del(key);
    
}

