/*global async: true, require: true, console: true, process: true, logger: true, messages: true, SuccessWrapper: true, jwt: true, secret: true, config: true, ErrorWrapper: true, module: true, params: true  */

/**
 * Created by Dhiman on 2/1/2017.
*/

var util = require('util');

function CustomError(code, res) {
    
    var response = {};
    response.code = code;
    response.result = {};
    response.message = messages.statusMessages[code];
    
    res.send(response);
    
}


///--- Exports
module.exports = {
    CustomError: CustomError
};