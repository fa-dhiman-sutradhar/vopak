/*global async: true, require: true, console: true, process: true, logger: true, messages: true, SuccessWrapper: true, jwt: true, secret: true, config: true, ErrorWrapper: true, module: true, params: true  */

/**
 * Created by Dhiman on 2/1/2017.
*/

function SuccessResponse(code, res, responseObj, message) {
    var response = {};
    response.code = code;
    response.result = responseObj;
    if (code != 200) {
        response.message = messages.statusMessages[code];
    } else {
        response.message = message;
    }
    
    res.send(response);
}



///--- Exports
module.exports = {
    SuccessResponse: SuccessResponse
};