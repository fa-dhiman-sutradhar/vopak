/*global async: true, require: true, console: true, process: true, logger: true, messages: true, SuccessWrapper: true, jwt: true, secret: true, config: true, ErrorWrapper: true, module: true, params: true  */

/**
 * Created by Dhiman on 2/1/2017.
*/


var mongoose = require('mongoose');

//Mongo Database for Production server
var server = config.production;

//Mongo Database for Test server
if(process.env.SERVER_ENV == "stage") {
    server = config.stage;
}

//Mongo Database for Test server
if(process.env.SERVER_ENV == "local") {
    server = config.local;
}

var option = {
    user: server.username,
    pass: server.password
};

mongoose.connect(server.dbPath, option);
var db = mongoose.connection;

//Operations to perform when error while mongo connection
db.on('error', function (err) {
	console.log('error occured from db ' + err);
	logger.warn('error occured while connecting to mongo db ' + err);
    console.log(process)
    mongoose.connect(server.dbPath);
});
 

//Operations to perform when mongo is connected
db.on('connected', function connectedDb() {
	logger.warn('mongo connected successfully');
	console.log('mongo connected successfully' + server.dbPath);
});


//Operations to perform when mongo is disconnected
db.on('disconnected', function disconnectedDb() {
	logger.warn('mongo disconnected');
	console.log('mongo disconnected');
	mongoose.connect(server.dbPath, option);
});
 
exports.mongoose = mongoose;