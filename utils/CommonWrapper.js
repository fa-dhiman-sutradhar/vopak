/*global async: true, require: true, console: true, process: true, logger: true, messages: true, SuccessWrapper: true, jwt: true, secret: true, config: true, ErrorWrapper: true, module: true, params: true  */

/**
 * Created by Dhiman on 2/1/2017.
*/


var self = {
    /**
    * Method for getting related users
    * Input params post userId, callback
    * Response back related user ids array
    **/

    generateSalt : function(){
        
        Crypto.randomBytes('256', function(err, buf) {
            if (err) throw err;
            return buf;
        });
    }

};

module.exports = self;
